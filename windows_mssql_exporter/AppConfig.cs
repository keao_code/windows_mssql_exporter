﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace windows_mssql_exporter
{
    public class AppConfig
    {
        static AppConfig()
        {
            if (File.Exists("appsettings.json"))
            {
                AppConfig config= System.Text.Json.JsonSerializer.Deserialize<AppConfig>(File.ReadAllText("appsettings.json"));
                if (config != null)
                {
                    Current = config;
                }
            }
        }

        public static AppConfig Current { get; private set; }

        public string DataSource { get; set; }
    }
}
